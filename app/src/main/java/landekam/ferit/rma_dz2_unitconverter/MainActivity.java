package landekam.ferit.rma_dz2_unitconverter;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

public class MainActivity extends AppCompatActivity {

    ImageButton ibDistance, ibMass, ibTemperature, ibVolume;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initializeUI();
    }

    private void initializeUI() {
        this.ibDistance = findViewById(R.id.IBDistance);
        this.ibMass = findViewById(R.id.IBMass);
        this.ibTemperature = findViewById(R.id.IBTemperature);
        this.ibVolume = findViewById(R.id.IBVolume);

        ibDistance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StartDistanceConversionActivity();
            }
        });

        ibMass.setOnClickListener((new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StartMassConversionActivity();
            }
        }));

        ibTemperature.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StartTemperatureConversionActivity();
            }
        });

        ibVolume.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StartVolumeConversionActivity();
            }
        });
    }

    private void StartVolumeConversionActivity() {
        Intent intent = new Intent(this, VolumeConversionActivity.class);
        startActivity(intent);
    }

    private void StartTemperatureConversionActivity() {
        Intent intent = new Intent(this, TemperatureConversionActivity.class);
        startActivity(intent);
    }

    private void StartMassConversionActivity() {
        Intent intent = new Intent(this, MassConversionActivity.class);
        startActivity(intent);
    }

    private void StartDistanceConversionActivity() {
        Intent intent = new Intent(this, DistanceConversionActivity.class);
        startActivity(intent);
    }

}
