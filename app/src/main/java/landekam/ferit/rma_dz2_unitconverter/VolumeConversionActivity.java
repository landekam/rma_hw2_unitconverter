package landekam.ferit.rma_dz2_unitconverter;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

public class VolumeConversionActivity extends AppCompatActivity {

    Spinner sUnit1, sUnit2;
    EditText etInput;
    Button bConvert;
    TextView tvResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_volume_conversion);
        initializeUI();
    }

    private void initializeUI() {
        this.sUnit1 = findViewById(R.id.sUnit1Volume);
        this.sUnit2 = findViewById(R.id.sUnit2Volume);
        this.etInput = findViewById(R.id.etInputVolume);
        this.bConvert = findViewById(R.id.bConvertVolume);
        this.tvResult = findViewById(R.id.tvResultVolume);

        ArrayAdapter<CharSequence> arrayAdapter = ArrayAdapter.createFromResource(this,R.array.volume, android.R.layout.simple_spinner_item);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sUnit1.setAdapter(arrayAdapter);
        sUnit2.setAdapter(arrayAdapter);

        bConvert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Convert();
            }
        });
    }

    private void Convert() {
        int unit1 = sUnit1.getSelectedItemPosition();
        int unit2 = sUnit2.getSelectedItemPosition();
        double input;
        try {
            input = Double.parseDouble(etInput.getText().toString());
        }
        catch (Exception e){
            input = 0.0;
        }
        double result =  0.0;

        if (unit1 == 0) {
            if (unit2 == 0) {
                result = input;
            } else if (unit2 == 1) {
                result = MetricSmallerToLarger1000Times(input);
            } else if (unit2 == 2) {
                result = MetricSmallerToLarger1000Times(input);
                result = MetricSmallerToLarger1000Times(result);
            } else if(unit2 == 3){
                result = CubicCentimetersToPints(input);
            } else if(unit2 == 4){
                result = CubicCentimetersToPints(input);
                result = PintsToGallons(result);
            }
        }
        else if (unit1 == 1){
            if (unit2 == 0) {
                result = MetricLargerToSmaller1000Times(input);
            } else if (unit2 == 1) {
                result = input;
            } else if (unit2 == 2) {
                result = MetricSmallerToLarger1000Times(input);
            } else if(unit2 == 3){
                result = MetricLargerToSmaller1000Times(input);
                result = CubicCentimetersToPints(result);
            } else if(unit2 == 4){
                result = MetricLargerToSmaller1000Times(input);
                result = CubicCentimetersToPints(result);
                result = PintsToGallons(result);
            }
        }
        else if (unit1 == 2){
            if (unit2 == 0) {
                result = MetricLargerToSmaller1000Times(input);
                result = MetricLargerToSmaller1000Times(result);
            } else if (unit2 == 1) {
                result = MetricLargerToSmaller1000Times(input);
            } else if (unit2 == 2) {
                result = input;
            } else if(unit2 == 3){
                result = MetricLargerToSmaller1000Times(input);
                result = MetricLargerToSmaller1000Times(result);
                result = CubicCentimetersToPints(result);
            } else if(unit2 == 4){
                result = MetricLargerToSmaller1000Times(input);
                result = MetricLargerToSmaller1000Times(result);
                result = CubicCentimetersToPints(result);
                result = PintsToGallons(result);
            }
        }
        else if (unit1 == 3){
            if (unit2 == 0) {
                result = PintsToCubicCentimeters(input);
            } else if (unit2 == 1) {
                result = PintsToCubicCentimeters(input);
                result = MetricSmallerToLarger1000Times(result);
            } else if (unit2 == 2) {
                result = PintsToCubicCentimeters(input);
                result = MetricSmallerToLarger1000Times(result);
                result = MetricSmallerToLarger1000Times(result);
            } else if(unit2 == 3){
                result = input;
            } else if(unit2 == 4){
                result = PintsToGallons(input);
            }
        }
        else if (unit1 == 4){
            if (unit2 == 0) {
                result = GallonsToPints(input);
                result = PintsToCubicCentimeters(result);
            } else if (unit2 == 1) {
                result = GallonsToPints(input);
                result = PintsToCubicCentimeters(result);
                result = MetricSmallerToLarger1000Times(result);
            } else if (unit2 == 2) {
                result = GallonsToPints(input);
                result = PintsToCubicCentimeters(result);
                result = MetricSmallerToLarger1000Times(result);
                result = MetricSmallerToLarger1000Times(result);
            } else if(unit2 == 3){
                result = GallonsToPints(input);
            } else if(unit2 == 4){
                result = input;
            }
        }
        String stringResult = Double.toString(result);
        tvResult.setText(stringResult + " " + sUnit2.getSelectedItem());
    }

    private double MetricLargerToSmaller1000Times(double input) {
        return input * 1000;
    }

    private double MetricSmallerToLarger1000Times(double input) {
        return input / 1000;
    }

    private double CubicCentimetersToPints(double input) { return input / 473.176473;}

    private double PintsToCubicCentimeters(double input) { return input * 473.176473;}

    private double PintsToGallons (double input) { return input / 8;}

    private double GallonsToPints (double input) { return input * 8;}
}
