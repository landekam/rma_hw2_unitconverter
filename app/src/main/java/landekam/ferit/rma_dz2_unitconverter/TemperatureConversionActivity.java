package landekam.ferit.rma_dz2_unitconverter;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

public class TemperatureConversionActivity extends AppCompatActivity {

    Spinner sUnit1, sUnit2;
    EditText etInput;
    Button bConvert;
    TextView tvResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_temperature_conversion);
        initializeUI();
    }

    private void initializeUI() {
        this.sUnit1 = findViewById(R.id.sUnit1Temperature);
        this.sUnit2 = findViewById(R.id.sUnit2Temperature);
        this.etInput = findViewById(R.id.etInputTemperature);
        this.bConvert = findViewById(R.id.bConvertTemperature);
        this.tvResult = findViewById(R.id.tvResultTemperature);

        ArrayAdapter<CharSequence> arrayAdapter = ArrayAdapter.createFromResource(this,R.array.temperature, android.R.layout.simple_spinner_item);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sUnit1.setAdapter(arrayAdapter);
        sUnit2.setAdapter(arrayAdapter);

        bConvert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Convert();
            }
        });
    }

    private void Convert() {
        int unit1 = sUnit1.getSelectedItemPosition();
        int unit2 = sUnit2.getSelectedItemPosition();
        double input;
        try {
            input = Double.parseDouble(etInput.getText().toString());
        }
        catch (Exception e){
            input = 0.0;
        }
        double result =  0.0;

        if (unit1 == 0){
            if (unit2 == 0){
                result = input;
            }
            else if(unit2 == 1){
                result = CelsiusToKelvin(input);
            }
            else if(unit2 == 2){
                result = CelsiusToFahrenheit(input);
            }
        }

        else if(unit1 == 1){
            if (unit2 == 0){
                result = KelvinToCelsius(input);
            }
            else if(unit2 == 1){
                result = input;
            }
            else if(unit2 == 2){
                result = KelvinToCelsius(input);
                result = CelsiusToFahrenheit(result);
            }
        }

        else if(unit1 == 2){
            if (unit2 == 0){
                result = FahrenheitToCelsius(input);
            }
            else if(unit2 == 1){
                result = FahrenheitToCelsius(input);
                result = CelsiusToKelvin(result);
            }
            else if(unit2 == 2){
                result = input;
            }
        }
        String stringResult = Double.toString(result);
        tvResult.setText(stringResult + " " + sUnit2.getSelectedItem());

    }

    private double CelsiusToKelvin(double input){
        return input + 274.15;
    }

    private double KelvinToCelsius(double input){
        return input - 274.15;
    }

    private double CelsiusToFahrenheit(double input){
        return input * 1.8 + 32;
    }

    private double FahrenheitToCelsius(double input){
        return (input - 32) / 1.8;
    }
}
