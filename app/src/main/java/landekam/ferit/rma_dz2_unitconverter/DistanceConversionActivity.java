package landekam.ferit.rma_dz2_unitconverter;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

public class DistanceConversionActivity extends AppCompatActivity {

    Spinner sUnit1, sUnit2;
    EditText etInput;
    Button bConvert;
    TextView tvResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_distance_conversion);
        initializeUI();
    }

    private void initializeUI() {
        this.sUnit1 = findViewById(R.id.sUnit1Distance);
        this.sUnit2 = findViewById(R.id.sUnit2Distance);
        this.etInput = findViewById(R.id.etInputDistance);
        this.bConvert = findViewById(R.id.bConvertDistance);
        this.tvResult = findViewById(R.id.tvResultDistance);

        ArrayAdapter<CharSequence> arrayAdapter = ArrayAdapter.createFromResource(this,R.array.distance, android.R.layout.simple_spinner_item);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sUnit1.setAdapter(arrayAdapter);
        sUnit2.setAdapter(arrayAdapter);

        bConvert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Convert();
            }
        });
    }

    private void Convert() {
        int unit1 = sUnit1.getSelectedItemPosition();
        int unit2 = sUnit2.getSelectedItemPosition();
        double input;
        try {
            input = Double.parseDouble(etInput.getText().toString());
        }
        catch (Exception e){
            input = 0.0;
        }
        double result =  0.0;

        if (unit1 == 0){
            if (unit2 == 0){
                result = input;
            }
            else if (unit2 == 1){
                result = MetricSmallerToLarger10Times(input);
            }
            else if (unit2 == 2){
                result = MetricSmallerToLarger10Times(input);
                result = MetricSmallerToLarger10Times(result);
            }
            else if (unit2 == 3){
                result = MetricSmallerToLarger1000Times(input);
            }
            else if (unit2 == 4){
                result = MetricSmallerToLarger1000Times(input);
                result = MetricSmallerToLarger1000Times(result);
            }
            else if (unit2 == 5){
                result = MilimetersToInches(input);
            }
            else if (unit2 == 6){
                result = MilimetersToInches(input);
                result = InchesToFeet(result);
            }
            else if (unit2 == 7){
                result = MilimetersToInches(input);
                result = FeetToMiles(result);
            }
        }
        else if (unit1 == 1){
            if (unit2 == 0){
                result = MetricLargerToSmaller10Times(input);
            }
            else if (unit2 == 1){
                result = input;
            }
            else if (unit2 == 2){
                result = MetricSmallerToLarger10Times(input);
            }
            else if (unit2 == 3){
                result = MetricSmallerToLarger10Times(input);
                result = MetricSmallerToLarger10Times(result);
            }
            else if (unit2 == 4){
                result = MetricSmallerToLarger10Times(input);
                result = MetricSmallerToLarger10Times(result);
                result = MetricSmallerToLarger1000Times(result);
            }
            else if (unit2 == 5){
                result = MetricLargerToSmaller10Times(input);
                result = MilimetersToInches(result);
            }
            else if (unit2 == 6){
                result = MetricLargerToSmaller10Times(input);
                result = MilimetersToInches(result);
                result = InchesToFeet(result);
            }
            else if (unit2 == 7){
                result = MetricLargerToSmaller10Times(input);
                result = MilimetersToInches(result);
                result = InchesToFeet(result);
                result = FeetToMiles(result);
            }
        }
        else if (unit1 == 2){
            if (unit2 == 0){
                result = MetricLargerToSmaller10Times(input);
                result = MetricLargerToSmaller10Times(result);
            }
            else if (unit2 == 1){
                result = MetricLargerToSmaller10Times(input);
            }
            else if (unit2 == 2){
                result = input;
            }
            else if (unit2 == 3){
                result = MetricSmallerToLarger10Times(input);
            }
            else if (unit2 == 4){
                result = MetricSmallerToLarger10Times(input);
                result = MetricSmallerToLarger1000Times(result);
            }
            else if (unit2 == 5){
                result = MetricLargerToSmaller10Times(input);
                result = MetricLargerToSmaller10Times(result);
                result = MilimetersToInches(result);
            }
            else if (unit2 == 6){
                result = MetricLargerToSmaller10Times(input);
                result = MetricLargerToSmaller10Times(result);
                result = MilimetersToInches(result);
                result = InchesToFeet(result);
            }
            else if (unit2 == 7){
                result = MetricLargerToSmaller10Times(input);
                result = MetricLargerToSmaller10Times(result);
                result = MilimetersToInches(result);
                result = InchesToFeet(result);
                result = FeetToMiles(result);
            }
        }
        else if (unit1 == 3){
            if (unit2 == 0){
                result = MetricLargerToSmaller1000Times(input);
            }
            else if (unit2 == 1){
                result = MetricLargerToSmaller10Times(input);
                result = MetricLargerToSmaller10Times(result);
            }
            else if (unit2 == 2){
                result = MetricLargerToSmaller10Times(input);
            }
            else if (unit2 == 3){
                result = input;
            }
            else if (unit2 == 4){
                result = MetricSmallerToLarger1000Times(input);
            }
            else if (unit2 == 5){
                result = MetricLargerToSmaller1000Times(input);
                result = MilimetersToInches(result);
            }
            else if (unit2 == 6){
                result = MetricLargerToSmaller1000Times(input);
                result = MilimetersToInches(result);
                result = InchesToFeet(result);
            }
            else if (unit2 == 7){
                result = MetricLargerToSmaller1000Times(input);
                result = MilimetersToInches(result);
                result = InchesToFeet(result);
                result = FeetToMiles(result);
            }
        }
        else if (unit1 == 4){
            if (unit2 == 0){
                result = MetricLargerToSmaller1000Times(input);
                result = MetricLargerToSmaller1000Times(result);
            }
            else if (unit2 == 1){
                result = MetricLargerToSmaller1000Times(input);
                result = MetricLargerToSmaller10Times(result);
                result = MetricLargerToSmaller10Times(result);
            }
            else if (unit2 == 2){
                result = MetricLargerToSmaller1000Times(input);
                result = MetricLargerToSmaller10Times(result);
            }
            else if (unit2 == 3){
                result = MetricLargerToSmaller1000Times(input);
            }
            else if (unit2 == 4){
                result = input;
            }
            else if (unit2 == 5){
                result = MetricLargerToSmaller1000Times(input);
                result = MetricLargerToSmaller1000Times(result);
                result = MilimetersToInches(result);
            }
            else if (unit2 == 6){
                result = MetricLargerToSmaller1000Times(input);
                result = MetricLargerToSmaller1000Times(result);
                result = MilimetersToInches(result);
                result = InchesToFeet(result);
            }
            else if (unit2 == 7){
                result = MetricLargerToSmaller1000Times(input);
                result = MetricLargerToSmaller1000Times(result);
                result = MilimetersToInches(result);
                result = InchesToFeet(result);
                result = FeetToMiles(result);
            }
        }
        else if (unit1 == 5){
            if (unit2 == 0){
                result = InchesToMilimeters(input);
            }
            else if (unit2 == 1){
                result = InchesToMilimeters(input);
                result = MetricSmallerToLarger10Times(result);
            }
            else if (unit2 == 2){
                result = InchesToMilimeters(input);
                result = MetricSmallerToLarger10Times(result);
                result = MetricSmallerToLarger10Times(result);
            }
            else if (unit2 == 3){
                result = InchesToMilimeters(input);
                result = MetricSmallerToLarger1000Times(result);
            }
            else if (unit2 == 4){
                result = InchesToMilimeters(input);
                result = MetricSmallerToLarger1000Times(result);
                result = MetricSmallerToLarger1000Times(result);
            }
            else if (unit2 == 5){
                result = input;
            }
            else if (unit2 == 6){
                result = InchesToFeet(input);
            }
            else if (unit2 == 7){
                result = InchesToFeet(input);
                result = FeetToMiles(result);
            }
        }
        else if (unit1 == 6){
            if (unit2 == 0){
                result = FeetToInches(input);
                result = InchesToMilimeters(result);
            }
            else if (unit2 == 1){
                result = FeetToInches(input);
                result = InchesToMilimeters(result);
                result = MetricSmallerToLarger10Times(result);
            }
            else if (unit2 == 2){
                result = FeetToInches(input);
                result = InchesToMilimeters(result);
                result = MetricSmallerToLarger10Times(result);
                result = MetricSmallerToLarger10Times(result);
            }
            else if (unit2 == 3){
                result = FeetToInches(input);
                result = InchesToMilimeters(result);
                result = MetricSmallerToLarger1000Times(result);
            }
            else if (unit2 == 4){
                result = FeetToInches(input);
                result = InchesToMilimeters(result);
                result = MetricSmallerToLarger1000Times(result);
                result = MetricSmallerToLarger1000Times(result);
            }
            else if (unit2 == 5){
                result = FeetToInches(input);
            }
            else if (unit2 == 6){
                result = input;
            }
            else if (unit2 == 7){
                result = FeetToMiles(input);
            }
        }
        else if (unit1 == 7){
            if (unit2 == 0){
                result = MilesToFeet(input);
                result = FeetToInches(result);
                result = InchesToMilimeters(result);
            }
            else if (unit2 == 1){
                result = MilesToFeet(input);
                result = FeetToInches(result);
                result = InchesToMilimeters(result);
                result = MetricSmallerToLarger10Times(result);
            }
            else if (unit2 == 2){
                result = MilesToFeet(input);
                result = FeetToInches(result);
                result = InchesToMilimeters(result);
                result = MetricSmallerToLarger10Times(result);
                result = MetricSmallerToLarger10Times(result);
            }
            else if (unit2 == 3){
                result = MilesToFeet(input);
                result = FeetToInches(result);
                result = InchesToMilimeters(result);
                result = MetricSmallerToLarger1000Times(result);
            }
            else if (unit2 == 4){
                result = MilesToFeet(input);
                result = FeetToInches(result);
                result = InchesToMilimeters(result);
                result = MetricSmallerToLarger1000Times(result);
                result = MetricSmallerToLarger1000Times(result);
            }
            else if (unit2 == 5){
                result = MilesToFeet(input);
                result = FeetToInches(result);
            }
            else if (unit2 == 6){
                result = MilesToFeet(input);
            }
            else if (unit2 == 7){
                result = input;
            }
        }
        String stringResult = Double.toString(result);
        tvResult.setText(stringResult + " " + sUnit2.getSelectedItem());
    }

    private double MilesToFeet(double input) {
        return input * 5280;
    }

    private double FeetToInches(double input) {
        return input * 12;
    }

    private double InchesToMilimeters(double input) {
        return input * 25.4;
    }

    private double MetricLargerToSmaller1000Times(double input) {
        return input * 1000;
    }

    private double MetricLargerToSmaller10Times(double input) {
        return input * 10;
    }

    private double FeetToMiles(double input) {
        return input / 5280;
    }

    private double InchesToFeet(double input) {
        return input / 12;
    }

    private double MilimetersToInches(double input) {
        return input / 25.4;
    }

    private double MetricSmallerToLarger1000Times(double input) {
        return input / 1000;
    }

    private double MetricSmallerToLarger10Times(double input) {
        return input / 10;
    }

}
