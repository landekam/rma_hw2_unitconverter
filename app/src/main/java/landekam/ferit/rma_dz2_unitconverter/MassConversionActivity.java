package landekam.ferit.rma_dz2_unitconverter;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

public class MassConversionActivity extends AppCompatActivity {

    Spinner sUnit1, sUnit2;
    EditText etInput;
    Button bConvert;
    TextView tvResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mass_conversion);
        initializeUI();
    }

    private void initializeUI() {
        this.sUnit1 = findViewById(R.id.sUnit1Mass);
        this.sUnit2 = findViewById(R.id.sUnit2Mass);
        this.etInput = findViewById(R.id.etInputMass);
        this.bConvert = findViewById(R.id.bConvertMass);
        this.tvResult = findViewById(R.id.tvResultMass);

        ArrayAdapter<CharSequence> arrayAdapter = ArrayAdapter.createFromResource(this,R.array.mass, android.R.layout.simple_spinner_item);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sUnit1.setAdapter(arrayAdapter);
        sUnit2.setAdapter(arrayAdapter);

        bConvert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Convert();
            }
        });
    }

    private void Convert() {
        int unit1 = sUnit1.getSelectedItemPosition();
        int unit2 = sUnit2.getSelectedItemPosition();
        double input;
        try {
            input = Double.parseDouble(etInput.getText().toString());
        }
        catch (Exception e){
            input = 0.0;
        }
        double result =  0.0;
        if (unit1 == 0){
            if (unit2 == 0){
                result = input;
            }
            else if (unit2 == 1){
                result = MetricSmallerToLarger10Times(input);
            }
            else if (unit2 == 2){
                result = MetricSmallerToLarger1000Times(input);
            }
            else if (unit2 == 3){
                result = MetricSmallerToLarger1000Times(input);
                result = MetricSmallerToLarger1000Times(result);
            }
            else if (unit2 == 4){
                result = GramsToOunces(input);
            }
            else if (unit2 == 5){
                result = GramsToOunces(input);
                result = OuncesToPounds(result);
            }
            else if (unit2 == 6){
                result = GramsToOunces(input);
                result = OuncesToPounds(result);
                result = PoundsToImperialTons(result);
            }
        }
        else if (unit1 == 1){
            if (unit2 == 0){
                result = MetricLargerToSmaller10Times(input);
            }
            else if (unit2 == 1){
                result = input;
            }
            else if (unit2 == 2){
                result = MetricSmallerToLarger100Times(input);
            }
            else if (unit2 == 3){
                result = MetricSmallerToLarger100Times(input);
                result = MetricSmallerToLarger1000Times(result);
            }
            else if (unit2 == 4){
                result = MetricLargerToSmaller10Times(input);
                result = GramsToOunces(result);
            }
            else if (unit2 == 5){
                result = MetricLargerToSmaller10Times(input);
                result = GramsToOunces(result);
                result = OuncesToPounds(result);
            }
            else if (unit2 == 6){
                result = MetricLargerToSmaller10Times(input);
                result = GramsToOunces(result);
                result = OuncesToPounds(result);
                result = PoundsToImperialTons(result);
            }
        }
        else if (unit1 == 2){
            if (unit2 == 0){
                result = MetricLargerToSmaller1000Times(input);
            }
            else if (unit2 == 1){
                result = MetricLargerToSmaller100Times(input);
            }
            else if (unit2 == 2){
                result = input;
            }
            else if (unit2 == 3){
                result = MetricSmallerToLarger1000Times(input);
            }
            else if (unit2 == 4){
                result = MetricLargerToSmaller1000Times(input);
                result = GramsToOunces(result);
            }
            else if (unit2 == 5){
                result = MetricLargerToSmaller1000Times(input);
                result = GramsToOunces(result);
                result = OuncesToPounds(result);
            }
            else if (unit2 == 6){
                result = MetricLargerToSmaller1000Times(input);
                result = GramsToOunces(result);
                result = OuncesToPounds(result);
                result = PoundsToImperialTons(result);
            }
        }
        else if (unit1 == 3){
            if (unit2 == 0){
                result = MetricLargerToSmaller1000Times(input);
                result = MetricLargerToSmaller1000Times(result);
            }
            else if (unit2 == 1){
                result = MetricLargerToSmaller1000Times(input);
                result = MetricLargerToSmaller100Times(result);
            }
            else if (unit2 == 2){
                result = MetricLargerToSmaller1000Times(input);
            }
            else if (unit2 == 3){
                result = input;
            }
            else if (unit2 == 4){
                result = MetricLargerToSmaller1000Times(input);
                result = MetricLargerToSmaller1000Times(result);
                result = GramsToOunces(result);
            }
            else if (unit2 == 5){
                result = MetricLargerToSmaller1000Times(input);
                result = MetricLargerToSmaller1000Times(result);
                result = GramsToOunces(result);
                result = OuncesToPounds(result);
            }
            else if (unit2 == 6){
                result = MetricLargerToSmaller1000Times(input);
                result = MetricLargerToSmaller1000Times(result);
                result = GramsToOunces(result);
                result = OuncesToPounds(result);
                result = PoundsToImperialTons(result);
            }
        }
        else if (unit1 == 4){
            if (unit2 == 0){
                result = OuncesToGrams(input);
            }
            else if (unit2 == 1){
                result = OuncesToGrams(input);
                result = MetricSmallerToLarger10Times(result);
            }
            else if (unit2 == 2){
                result = OuncesToGrams(input);
                result = MetricSmallerToLarger1000Times(result);
            }
            else if (unit2 == 3){
                result = OuncesToGrams(input);
                result = MetricSmallerToLarger1000Times(result);
                result = MetricSmallerToLarger1000Times(result);
            }
            else if (unit2 == 4){
                result = input;
            }
            else if (unit2 == 5){
                result = OuncesToPounds(input);
            }
            else if (unit2 == 6){
                result = OuncesToPounds(result);
                result = PoundsToImperialTons(result);
            }
        }
        else if (unit1 == 5){
            if (unit2 == 0){
                result = PoundsToOunces(input);
                result = OuncesToGrams(result);
            }
            else if (unit2 == 1){
                result = PoundsToOunces(input);
                result = OuncesToGrams(result);
                result = MetricSmallerToLarger10Times(result);
            }
            else if (unit2 == 2){
                result = PoundsToOunces(input);
                result = OuncesToGrams(result);
                result = MetricSmallerToLarger1000Times(result);
            }
            else if (unit2 == 3){
                result = PoundsToOunces(input);
                result = OuncesToGrams(result);
                result = MetricSmallerToLarger1000Times(result);
                result = MetricSmallerToLarger1000Times(result);
            }
            else if (unit2 == 4){
                result = PoundsToOunces(input);
            }
            else if (unit2 == 5){
                result = input;
            }
            else if (unit2 == 6){
                result = PoundsToImperialTons(input);
            }
        }
        else if (unit1 == 6){
            if (unit2 == 0){
                result = ImperialTonsToPounds(input);
                result = PoundsToOunces(result);
                result = OuncesToGrams(result);
            }
            else if (unit2 == 1){
                result = ImperialTonsToPounds(input);
                result = PoundsToOunces(result);
                result = OuncesToGrams(result);
                result = MetricSmallerToLarger10Times(result);
            }
            else if (unit2 == 2){
                result = ImperialTonsToPounds(input);
                result = PoundsToOunces(result);
                result = OuncesToGrams(result);
                result = MetricSmallerToLarger1000Times(result);
            }
            else if (unit2 == 3){
                result = ImperialTonsToPounds(input);
                result = PoundsToOunces(result);
                result = OuncesToGrams(result);
                result = MetricSmallerToLarger1000Times(result);
                result = MetricSmallerToLarger1000Times(result);
            }
            else if (unit2 == 4){
                result = ImperialTonsToPounds(input);
                result = PoundsToOunces(result);
            }
            else if (unit2 == 5){
                result = ImperialTonsToPounds(input);
            }
            else if (unit2 == 6){
                result = input;
            }
        }
        String stringResult = Double.toString(result);
        tvResult.setText(stringResult + " " + sUnit2.getSelectedItem());
    }

    private double MetricLargerToSmaller1000Times(double input) {
        return input * 1000;
    }

    private double MetricLargerToSmaller100Times(double input) {
        return input * 100;
    }

    private double MetricLargerToSmaller10Times(double input) {
        return input * 10;
    }

    private double MetricSmallerToLarger1000Times(double input) {
        return input / 1000;
    }

    private double MetricSmallerToLarger100Times(double input) {
        return input / 100;
    }

    private double MetricSmallerToLarger10Times(double input) {
        return input / 10;
    }

    private double GramsToOunces(double input){
        return input / 28.34952;
    }

    private double OuncesToGrams(double input){
        return input * 28.34952;
    }

    private double OuncesToPounds(double input){
        return input / 16;
    }

    private double PoundsToOunces(double input){
        return input * 16;
    }

    private double PoundsToImperialTons(double input){
        return  input / 2240;
    }

    private double ImperialTonsToPounds(double input){
        return  input * 2240;
    }
}
